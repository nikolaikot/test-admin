const { Strategy, ExtractJwt } = require('passport-jwt')

const db = require('../util/db')()

const options = {
  jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
  secretOrKey: 'secret',
}

module.exports = new Strategy(options, (payload, done) => {
  const userId = payload.userId
  const candidate = db.get('users').find({ _id: userId }).value()

  if (candidate) {
    done(null, candidate)
  } else {
    done(null, false)
  }
})
