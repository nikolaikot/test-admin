const shortid = require('shortid')

// const db = require('../util/db')()

module.exports = class Comment {
  constructor(name, text) {
    this.name = name
    this.text = text
    this._id = shortid.generate()
    this.date = new Date().toLocaleString()
  }
}
