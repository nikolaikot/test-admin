const shortid = require('shortid')

module.exports = class Post {
  constructor(title, text, imageUrl) {
    this._id = shortid.generate()
    this.title = title
    this.text = text
    this.imageUrl = imageUrl
    this.views = 0
    this.date = new Date().toLocaleString()
    this.comments = []
  }
}
