const shortid = require('shortid')
const bcrypt = require('bcrypt')

module.exports = class User {
  constructor(login, password) {
    this._id = shortid.generate()
    this.login = login
    this.password = bcrypt.hashSync(password, 10)
  }
}
