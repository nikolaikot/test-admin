let db = null

module.exports = () => {
  if (!db) {
    const low = require('lowdb')
    const FileSync = require('lowdb/adapters/FileSync')
    const adapter = new FileSync('db.json')
    db = low(adapter)
    console.log('db inited!')
    return db
  } else {
    console.log('reuse db!')
    return db
  }
}
