const bcrypt = require('bcrypt')
const jwt = require('jsonwebtoken')

const User = require('../models/user-model')
const db = require('../util/db')()

exports.postLogin = (req, res, next) => {
  const login = req.body.login
  const password = req.body.password

  const user = db.get('users').find({ login }).value()
  if (user) {
    const isPasswordCorrect = bcrypt.compareSync(password, user.password)

    if (isPasswordCorrect) {
      const token = jwt.sign(
        {
          login: user.login,
          userId: user._id,
        },
        'secret',
        { expiresIn: '1h' }
      )
      res.json(token)
    } else {
      res.status(401).json({ message: 'Wrong login or password' })
    }
  } else {
    res.status(401).json({ message: 'Wrong login or password' })
  }
}

exports.postCreateUser = (req, res, next) => {
  const login = req.body.login
  const password = req.body.password

  if (db.get('users').find({ login }).value()) {
    res.status(409).json({ message: 'This login already exists!' })
  } else {
    const user = new User(login, password)
    db.get('users').push(user).write()
    res.status(201).json({ message: 'User created successfully!' })
  }
}
