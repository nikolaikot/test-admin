const fs = require('fs')
const path = require('path')

const Post = require('../models/post-model')
const db = require('../util/db')()

exports.postCreatePost = (req, res, next) => {
  const title = req.body.title
  const text = req.body.text
  const imageUrl = `/${req.file.filename}`

  const post = new Post(title, text, imageUrl)
  db.get('posts').push(post).write()
  res.status(201).json(post)
}

exports.getGetAllPosts = (req, res, next) => {
  const posts = db.get('posts').value()
  res.json(posts)
}

exports.getGetSinglePost = (req, res, next) => {
  const postId = req.params.id
  const post = db.get('posts').find({ _id: postId }).value()
  res.json(post)
}

exports.putEditPost = (req, res, next) => {
  const postId = req.params.id
  const text = req.body.text
  db.get('posts').find({ _id: postId }).assign({ text }).write()
  res.json({ message: 'Post updated successfully!' })
}

exports.deleteRemovePost = (req, res, next) => {
  const postId = req.params.id
  const post = db.get('posts').find({ _id: postId }).value()
  clearImage(post.imageUrl)
  db.get('posts').remove({ _id: postId }).write()
  res.json({ message: 'Post deleted!' })
}

exports.putAddView = (req, res, next) => {
  const postId = req.params.id
  console.log(postId)
}

const clearImage = (filePath) => {
  filePath = path.join(__dirname, '../..', 'static', filePath)
  fs.unlink(filePath, (err) => console.log(err))
}
