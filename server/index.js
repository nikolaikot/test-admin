const express = require('express')
const bodyParser = require('body-parser')
const passport = require('passport')

const authRoutes = require('./routes/auth-routes')
const postRoutes = require('./routes/post-routes')
const commentRoutes = require('./routes/comment-routes')

const passportStrategy = require('./middleware/passport-strategy')

const app = express()

app.use(passport.initialize())
passport.use(passportStrategy)

app.use(bodyParser.json())

app.use('/auth', authRoutes)
app.use(postRoutes)
app.use(commentRoutes)

module.exports = {
  path: '/api',
  handler: app,
}
