const { Router } = require('express')
const passport = require('passport')

const postController = require('../controllers/post-controller')
const upload = require('../middleware/upload')

const router = Router()

router.post(
  '/post/admin',
  passport.authenticate('jwt', { session: false }),
  upload.single('image'),
  postController.postCreatePost
)

router.get(
  '/post/admin',
  passport.authenticate('jwt', { session: false }),
  postController.getGetAllPosts
)

router.get(
  '/post/admin/:id',
  passport.authenticate('jwt', { session: false }),
  postController.getGetSinglePost
)

router.put(
  '/post/admin/:id',
  passport.authenticate('jwt', { session: false }),
  postController.putEditPost
)

router.delete(
  '/post/admin/:id',
  passport.authenticate('jwt', { session: false }),
  postController.deleteRemovePost
)

// User routes

router.get('/post', postController.getGetAllPosts)

router.get('/post/:id', postController.getGetSinglePost)

router.put('/post/:id', postController.putAddView)

module.exports = router
