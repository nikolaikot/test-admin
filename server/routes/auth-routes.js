const { Router } = require('express')
const passport = require('passport')

const authController = require('../controllers/auth-controller')

const router = Router()

// /api/auth/admin/login
router.post('/admin/login', authController.postLogin)

router.post(
  '/admin/create',
  passport.authenticate('jwt', { session: false }),
  authController.postCreateUser
)

module.exports = router
