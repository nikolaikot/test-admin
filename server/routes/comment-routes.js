const { Router } = require('express')
const router = Router()

const commentController = require('../controllers/comment-controller')

router.post('/comment', commentController.postAddComment)

module.exports = router
