export const actions = {
  async fetchPosts({ commit }) {
    try {
      return await this.$axios.$get('/api/post')
    } catch (error) {
      commit('SET_ERROR', error, { root: true })
      throw error
    }
  },

  async fetchPost({ commit }, postId) {
    try {
      return await this.$axios.$get('/api/post/' + postId)
    } catch (error) {
      commit('SET_ERROR', error, { root: true })
      throw error
    }
  },

  async fetchAdminPosts({ commit }) {
    try {
      return await this.$axios.$get('/api/post/admin')
    } catch (error) {
      commit('SET_ERROR', error, { root: true })
      throw error
    }
  },

  async fetchAdminPost({ commit }, postId) {
    try {
      return await this.$axios.$get('/api/post/admin/' + postId)
    } catch (error) {
      commit('SET_ERROR', error, { root: true })
      throw error
    }
  },

  async create(context, form) {
    try {
      const fd = new FormData()
      fd.append('title', form.title)
      fd.append('text', form.text)
      fd.append('image', form.image, form.image.name)

      return await this.$axios.$post('/api/post/admin', fd)
    } catch (error) {
      context.commit('SET_ERROR', error, { root: true })
      throw error
    }
  },

  async remove({ commit }, id) {
    try {
      return await this.$axios.$delete('/api/post/admin/' + id)
    } catch (error) {
      commit('SET_ERROR', error, { root: true })
      throw error
    }
  },
  async update({ commit }, { id, text }) {
    try {
      return await this.$axios.$put('/api/post/admin/' + id, { text })
    } catch (error) {
      commit('SET_ERROR', error, { root: true })
      throw error
    }
  },
}
