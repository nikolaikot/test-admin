import Cookie from 'cookie'
import Cookies from 'js-cookie'
import jwt from 'jsonwebtoken'

export const state = () => ({
  token: null,
})

export const mutations = {
  SET_TOKEN(state, token) {
    state.token = token
  },
  CLEAR_TOKEN(state) {
    state.token = null
  },
}

export const actions = {
  async login({ commit, dispatch }, formData) {
    try {
      const token = await this.$axios.$post('/api/auth/admin/login', formData)
      dispatch('setToken', token)
    } catch (error) {
      commit('SET_ERROR', error, { root: true })
      throw error
    }
  },
  async createUser({ commit }, formData) {
    try {
      await this.$axios.$post('/api/auth/admin/create', formData)
    } catch (error) {
      commit('SET_ERROR', error, { root: true })
      throw error
    }
  },
  setToken({ commit }, token) {
    this.$axios.setToken(token, 'Bearer')
    commit('SET_TOKEN', token)
    Cookies.set('jwt-token', token)
  },
  logout({ commit }) {
    commit('CLEAR_TOKEN')
    this.$axios.setToken(false)
    Cookies.remove('jwt-token')
  },
  autoLogin({ dispatch }) {
    const cookieStr = process.browser
      ? document.cookie
      : this.app.context.req.headers.cookie

    const cookies = Cookie.parse(cookieStr || '') || {}
    const token = cookies['jwt-token']

    if (isJwtValid(token)) {
      dispatch('setToken', token)
    } else {
      dispatch('logout')
    }
  },
}

export const getters = {
  isAuthenticated: (state) => {
    return !!state.token
  },
  token: (state) => state.token,
}

function isJwtValid(token) {
  if (!token) {
    return false
  }

  const decoded = jwt.decode(token) || {}
  const expires = decoded.exp || 0

  return new Date().getTime() / 1000 < expires
}
